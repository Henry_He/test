# 节目元信息接口文档

## 节目元信息

### `incr_series`

| 字段 | 描述 | 取值范围 | 示例 |
|:-----|:----|:--------|:----|
| MediaCode | | | Umai:SERI/808813@BESTV.SMG.SMG |
| Name | | | 命中注定我爱你 |
| Type | | | 电视剧 |
| Genre | | | 励志剧;言情剧;偶像剧; |
| Tags | | | 现代;女性;成人; |
| Keywords | | | 偶像 情感 |
| Actors | | | 陈乔恩 阮经天 陈楚河 白歆惠 |
| Director | | | 陈铭章 |
| SourceType | | | BESTVIMSP |
| Region | | | NULL |
| Language | | | 0 |
| VolumnCount | | | 39 |
| ReleaseYear | | | 2012 |
| CreateTime | | | 2013-02-07 00:00:00 |
| UpdateTime | | | 2012-11-15 00:00:00 |
| Status | | | 0 |
| Price | | | 0.00 |

### `incr_program`

| 字段 | 描述 | 取值范围 | 示例 |
|:-----|:----|:--------|:----|
| MediaCode | | | Umai:PROG/116811@BESTV.SMG.SMG |
| Name | | | 命中注定我爱你[1] |
| SeriesCode | | | Umai:SERI/808813@BESTV.SMG.SMG |

## IPTV 平台

### `IMSP_CONTENT_BASE`

| 字段 | 描述 | 取值范围 | 示例 |
|:-----|:----|:--------|:----|
| CONTENT_ID | | | 770679 |
| NAME | | | 命中注定我爱你 |
| CODE | | | Umai:SERI/808813@BESTV.SMG.SMG |
| ORI_NAME | | | 命中注定我爱你New |
| TYPE | | | 3000 |
| GENRE | | | 励志剧;言情剧;偶像剧; |
| CP_CODE | | | BESTVIMSP |
| COPYRIGHT_TYPE | | | 0 |
| COPYRIGHT_VALID_TIME | | |  |
| COPYRIGHT_EXPIRE_TIME | | |  |
| DEFAULT_PRICE_TYPE | | | 0 |
| DEFAULT_PRICE | | | 0 |
| VALID_TIME | | | 2013-2-6.16.12. 0. 0 |
| EXPIRE_TIME | | | 2022-11-6.16.12. 58. 0 |
| KEYWORD | | | 偶像 情感 |
| TAGS | | | 现代;女性;成人; |
| SORT_NAME | | | mzzdwan 命中注定我爱你 |
| SRARCH_NAME | | | mzzdwan 命中注定我爱你 |
| NEW_DAY | | | 7 |
| LEFT_DAY | | |  |
| STATUS | | | 2 |
| ENABLE_STATUS | | | 1 |
| FILE_STATUS | | | 4 |
| LENGTH | | | 102601 |
| SOURCE | | | BESTVIMSP |
| CREATE_TIME | | | 2012-11-15.3.40. 34. 376000000 |
| UPDATE_TIME | | | 2013-2-7.11.40. 15. 744000000 |
| OPERATOR_ID | | |  |
| OLD_ID | | | 0 |
| CONTENT_TYPE | | | 电视剧 |
| ORDER_NUMBER | | |  |
| ACTIVE_STATUS | | | 1 |
| AUDIT_STATUS | | | 0 |
| EXTERNAL_ID | | | Umai:SERI/808813@BESTV.SMG.SMG |
| IS_PRICE_BAND | | | 1 |
| VOD_ARRANGE | | | 0 |
| CATEGORY | | | 电视剧看吧 |
| R_MEDIA_CODE | | |  |
| HD | | | 0 |

### `IMSP_CONTENT_SERIES`

| 字段 | 描述 | 取值范围 | 示例 |
|:-----|:----|:--------|:----|
| CONTENT_ID | | | 770679 |
| REGION | | |  |
| DESCIRPTION | | | 该剧创下台湾偶像剧神话，并获得了金钟奖等多个电视大奖。台湾偶像剧女王陈乔恩饰演“便利贴女孩”邂逅帅气王子阮经天，一趟豪华邮轮之旅，让这样一位平凡的女孩碰上史上最不平凡的际遇。 |
| VIEW_POINT | | |  |
| COMPANY | | |  |
| ISSUE_YEAR | | | 2012 |
| ORIGIN_DATE | | |  |
| LANGUAGE | | | 0 |
| COMMENTS | | |  |
| STAR_LEVEL | | | 1 |
| RATING | | | 1 |
| ACTOR_DISPLAY | | | 陈乔恩 阮经天 陈楚河 白歆惠 |
| WRITER_DISPLAY | | | 陈铭章 |
| AWARDS | | |  |
| EPISODE_NUMBER | | | 39 |
| ORDER_NUMBER | | |  |
| RESERVE1 | | |  |
| RESERVE2 | | |  |
| RESERVE3 | | |  |
| RESERVE4 | | |  |
| RESERVE5 | | |  |
| SERIES_PACKAGE | | | 20,29,30,31,32,27,38,39,2,14,33,34,3,10,11,12,1,21,22,23,24,9,7,13,26,8,19,5,15,36,18,17,25,37,35,4,6,28,16 |

### `IMSP_CONTENT_VOD`

| 字段 | 描述 | 取值范围 | 示例 |
|:-----|:----|:--------|:----|
| CONTENT_ID | | | 422490 |
| REGION | | | 美国 |
| DESCIRPTION | | | 银行家安迪因被当作杀害妻子与其情夫的凶手，被判终身监禁。安迪在监狱中一边帮监狱长做假账，一边精心策划了一出越狱好戏。 |
| VIEW_POINT | | |  |
| COMPANY | | | 其他; |
| ISSUE_YEAR | | | 2011 |
| ORIGIN_DATE | | |  |
| LANGUAGE | | | 0 |
| COMMENTS | | |  |
| STAR_LEVEL | | | 1 |
| RATING | | | 1 |
| ACTOR_DISPLAY | | | 蒂姆.罗宾斯 |
| WRITER_DISPLAY | | | 弗兰克.德拉邦特 |
| SPLITE_INFO | | |  |
| AWARDS | | |  |
| RESERVE1 | | |  |
| RESERVE2 | | |  |
| RESERVE3 | | |  |
| RESERVE4 | | |  |
| RESERVE5 | | |  |
| STORAGE_TYPE | | | 1 |

### `IMSP_REL_SERIES_EPISODE`

| 字段 | 描述 | 取值范围 | 示例 |
|:-----|:----|:--------|:----|
| ID | | | 20329 |
| EPISODE_ID | | | 89526 |
| SERIES_ID | | | 89525 |
| EPISODE_INDEX | | | 1 |
| STATUS | | | -1 |
| UPDATE_TIME | | | 2012-11-15.3.47. 35. 321000000 |
| OPERATOR_ID | | |  |
| ID | | | 86151 |
| EPISODE_ID | | | 89526 |
| SERIES_ID | | | 770679 |
| EPISODE_INDEX | | | 1 |
| STATUS | | | 0 |
| UPDATE_TIME | | | 2012-11-15.3.47. 35. 321000000 |
| OPERATOR_ID | | |  |

## OTT 平台

### `TV_CONTENT_BASE_EXT`

| 字段 | 描述 | 取值范围 | 示例 |
|:-----|:----|:--------|:----|
| CONTENT_ID | | | 1224631 |
| OLD_ID | | | Umai:SERI/808813@BESTV.SMG.SMG |
| ONLINE_STATUS | | | 2 |
| NAME | | | 命中注定我爱你 |
| TYPE | | | 3000 |
| CODE | | | 1226842 |
| CP_CODE | | | SMG |
| COPYRIGHT_TYPE | | | 0 |
| COPYRIGHT_VALID_TIME | | |  |
| COPYRIGHT_EXPIRE_TIME | | |  |
| VALID_TIME | | | 2012-11-6.16.12. 58. 0 |
| EXPIRE_TIME | | | 2022-11-6.16.12. 58. 0 |
| KEYWORD | | | 偶像 情感 |
| TAGS | | | 现代 |
| STATUS | | | 0 |
| OPERATOR_ID | | | 1560 |
| CREATE_TIME | | | 2012-11-15.15.30. 21. 364000000 |
| UPDATE_TIME | | | 2013-7-14.8.33. 54. 746000000 |
| ENABLE_STATUS | | |  |
| SOURCE | | |  |
| GENRE | | | 言情 偶像 |
| CONTENT_TYPE | | | series |
| TITLE | | | 命中注定我爱你 |
| IPD_TITLE | | | 命中注定我爱你 |
| PACKAGE_CODE | | |  |
| PRICE | | | 0 |
| SEARCH_NAME | | | mzzdwanNew |
| MOVIE_TYPE | | |  |
| BATCH_CODE | | |  |
| SUBTITLETYPE | | | 0 |
| SUBTITLEURL | | |  |
| REPOSITORY_STATUS | | | 0 |
| STAR_LEVEL | | | 8.5 |
| SERVICE_NAME | | |  |
| SERVICE_TYPE | | |  |
| AUDIT_STATUS | | | 2 |
| AUDIT_FALSE_STATUS | | |  |
| SUPPORT_PROTOCOL | | | 000000001100000011000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000011 |
| AUDITED_PROTOCOL | | | 000000002200000022000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000022 |
| ONLINED_PROTOCOL | | | 000000002200000022000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000022 |
| SUPPORT_BITERATE | | |  |
| MONTH_PLAY_TIMES1 | | | 0 |
| MONTH_PLAY_TIMES2 | | | 0 |
| WEEK_PLAY_TIMES1 | | | 0 |
| WEEK_PLAY_TIMES2 | | | 0 |
| DAY_PLAY_TIMES1 | | | 0 |
| DAY_PLAY_TIMES2 | | | 0 |
| TOTAL_PLAY_TIMES | | | 0 |
| ONLINED_BITRATE | | |  |
| P_CODE | | |  |
| R_MEDIA_CODE | | |  |
| IS_NEED_REPUBLISH | | | 0 |
| ORIGINAL | | |  |
| REQUEST_ID | | |  |

### `TV_CONTENT_SERIES_EXT`

| 字段 | 描述 | 取值范围 | 示例 |
|:-----|:----|:--------|:----|
| CONTENT_ID | | | 1224631 |
| REGION | | | 中国台湾 |
| DESCIRPTION | | | 该剧创下台湾偶像剧神话，并获得了金钟奖等多个电视大奖。台湾偶像剧女王陈乔恩饰演“便利贴女孩”邂逅帅气王子阮经天，一趟豪华邮轮之旅，让这样一位平凡的女孩碰上史上最不平凡的际遇。 |
| VIEW_POINT | | |  |
| COMPANY | | |  |
| ISSUE_YEAR | | | 2008 |
| ORIGIN_DATE | | |  |
| LANGUAGE | | | 国语 |
| COMMENTS | | |  |
| ACTOR_DISPLAY | | | 陈乔恩 阮经天 陈楚河 白歆惠 |
| WRITER_DISPLAY | | | 陈铭章 |
| EPISODE_NUMBER | | | 39 |
| LENGTH | | | 5026 |
| RESERVE1 | | |  |
| RESERVE2 | | |  |
| RESERVE3 | | |  |
| RESERVE4 | | |  |
| RESERVE5 | | |  |
| UPDATE_EPISODE_NUMBER | | | 0 |

### `TV_CONTENT_VOD_EXT`

| 字段 | 描述 | 取值范围 | 示例 |
|:-----|:----|:--------|:----|
| CONTENT_ID | | | 983395 |
| REGION | | | 美国 |
| DESCIRPTION | | | 银行家安迪因被当作杀害妻子与情夫的凶手，被判终身监禁。安迪在监狱中一方面帮监狱长做假账，一方面精心策划了一出越狱好戏。 |
| VIEW_POINT | | |  |
| COMPANY | | |  |
| ISSUE_YEAR | | | 1994 |
| ORIGIN_DATE | | |  |
| LANGUAGE | | | 英语 |
| COMMENTS | | |  |
| ACTOR_DISPLAY | | | 蒂姆·罗宾斯 摩根·弗里曼 鲍勃·冈顿 |
| WRITER_DISPLAY | | | 弗兰克·德拉邦特 |
| LENGTH | | | 8552 |
| IS_SERIES | | | 0 |
| RESERVE1 | | |  |
| RESERVE2 | | |  |
| RESERVE3 | | |  |
| RESERVE4 | | |  |
| RESERVE5 | | |  |

### `TV_MEDIA_FILE_EXT`

| 字段 | 描述 | 取值范围 | 示例 |
|:-----|:----|:--------|:----|
| ID | | | 2755210 |
| CODE | | | 2755210 |
| OLD_ID | | |  |
| CONTENT_CODE | | | 2824 |
| TYPE | | | 1 |
| EPISODE_NUM | | | 1 |
| SUPPORT_PROTOCOL | | | 0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010010000001 |
| AUDITED_PROTOCOL | | | 0000000022000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000020020000002 |
| ONLINED_PROTOCOL | | | 0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000020020000002 |
| BITRATE | | | 2300 |
| STATUS | | | 0000-00-00 00:00:00 |
| INSERT_TIME | | | 2013-6-8.9.33. 26. 318000000 |
| UPDATE_TIME | | | 2013-7-30.13.54. 35. 806000000 |
| OPERATOR_ID | | | 1021 |
| L_OPERATOR_ID | | |  |


