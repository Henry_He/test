# 百视通智能推荐系统部署手册

| 修订号 | 日期 | 描述 | 修订者 |
|:------|:-----|:----|:------|
| 5.0.0 | 2014-07-02 | 按推荐引擎第五版重新编写部署手册, 以 Markdown 格式编写. | 何永能 |

**注意 1** 在开始部署之前, 请在服务器中添加如下 host 配置. (`vim /etc/hosts`)
```{.texinfo}
10.61.13.107 gitlab.bestv.com
```

**注意 2** 配置软件源, 并安装常用软件, 详情参见附录[软件管理](#repo), 然后继续后面的步骤.

## 系统整体架构

### 网络拓扑图

![智能推荐的网络拓扑图](bmm_5.0.png)

### 服务器硬件配置

| 服务器角色 | 硬件配置 | 数量 | 测试 IP 地址 |
|:----------|:--------|:----|:------------|
| 建模服务器 | 32G 内存, 300Gx4 磁盘组 raid5 | 1 | 10.61.13.107 |
| 数据库服务器 | 32G 内存, 300Gx4 磁盘组 raid5 | 2 | 10.61.13.108/109 |
| Web 服务器 | 8G 内存, 300Gx2 磁盘组 raid1 | 2 | 10.61.13.110/111 |
| 预备服务器 | 8G 内存, 300Gx2 磁盘组 raid1 | 1 | 10.61.13.112 |

**注记** 预备服务器是用来预备做 Web 服务器的, 如果两台 Web 服务器的压力太大, 那么启用预备服务器.

## 部署建模服务器

### 系统环境

-   64 位 的 Red Hat Enterprise Linux Server release 6.x
-   MySQL 5.5 及以上版本
-   Java 1.7 及以上版本
-   Kettle 5.0 及以上版本

### 部署 C2 模块 {#bmm_c2}

`BMM_C2` 模块用于从驻地 CMS 数据库导出 CSV 节目数据文件.
选择一台能访问驻地 CMS 数据库的服务器, 一般选择 BMM 数据库服务器.
创建用户和密码都是 `bestvftp` 的 FTP 账户, 主目录为 `/view_log`, 命令如下:

```{.bash}
adduser -d '/view_log' bestvftp
passwd bestvftp # 输入两遍密码 bestvftp
```

如果没有安装 FTP 服务, 参照如下

```{.bash}
yum install -y vsftpd
service vsftpd start
chkconfig vsftpd on
```

创建工作目录 `/home/bestv`, 然后克隆 `BMM_C2` 模块代码

```{.bash}
mkdir -pv /home/bestv
cd /home/bestv
git clone http://gitlab.bestv.com/bmm/BMM_C2.git
cd BMM_C2
```

其中包含: `bestv-sync.jar` (主程序), `config` (配置文件), `start.bat` (启动脚本), 以及 `VERSION` (版本文件).
进入 `config` 文件夹, 根据是 IPTV 平台, 还是 OTT 平台, 拷贝对应的 `database.properties.*` 模板为 `database.properties`,
然后编辑该文件, 修改 CMS 数据库连接的 IP 地址.

**注意** 如果是 IPTV 平台, 还需编辑 `applicationContext.xml` 文件, 取消第 77 行的注释.

```
77                 <!-- <value>${datasync.program2}</value> -->
```

执行如下命令, 手动运行一次, 确保能成功导出数据

```{.bash}
java -jar bestv-sync.jar sync
```

如果导出成功, 将在 `/view_log` 路径生成两个 CSV 文件,
文件名以 `program_` 或 `series_` 打头, 紧跟 14 位数字时间戳, 例如 `series_20140721144046.csv`,
记录下前 10 位, 即 "年月日时", 这里为 `2014072114`, 然后将这个数字减去 1 得 `2014072113`,
将结果记录下来, 在[建立推荐系统 Master 数据库](#masterdb)小节会用得到.

最后, 一切正常, 启动 C2 程序, 该进程将一直运行, 以后每个整点导出一份增量数据

```{.bash}
./start.bat
```

查看日志

```{.bash}
tail -f /var/log/c2offline.log
```

### 安装并配置 MySQL 数据库

#### 安装数据库

-   安装 MySQL 服务器和客户端.
```{.bash}
yum -y install mysql mysql-server
service mysqld start
chkconfig mysqld on
```
-   设置 root 账户的密码为 `mysql`.
-   开放 3306 端口, 编辑 `/etc/sysconfig/iptables`, 在相应位置添加下列行
```{.texinfo}
-A INPUT -m state --state NEW -m tcp -p tcp --dport 3306 -j ACCEPT
```
重启防火墙服务 `service iptables restart`, 然后使用如下命令测试能否远程登陆数据库.
```{.bash}
mysql -uroot -pmysql -h10.61.13.108 --default-character-set=utf8
```

#### 配置数据库

配置 MySQL 数据库, 编辑文件 `/etc/my.cnf`, 可通过如下 SQL 语句查询全部配置信息.

```{.mysql}
show variables like '%engine%';
```

-   临时目录设置.
    在配置 MySQL 数据库时, 要确保临时目录所在分区有足够的磁盘空间,
    至少保证有 50G 以上空闲磁盘空间, 并且对 MySQL 数据库服务启动账户开放写权限.

    默认临时目录为 `/tmp`, 可通过如下配置修改目录指向,
    在 `[mysqld]` 下面加入 (可选)
```{.texinfo}
tmpdir=/var/mysqltmp
```

-   指定默认数据库引擎为 MyISAM.
    在 `[mysqld]` 下面加入
```{.texinfo}
default_storage_engine=MyISAM
```


-   索引相关的系统参数.
    在 `[mysqld]` 下面加入
```{.texinfo}
myisam_max_sort_file_size=9223372036853727232
```

### 建立推荐系统 Master 数据库 {#masterdb}

克隆数据库创建脚本.
```{.bash}
cd /home/bestv
git clone http://gitlab.bestv.com/bmm/BMM_DB.git
cd BMM_DB
```

根据实际情况, 修改 `deploy.sh` 文件, 然后执行.
该文件将完成以下任务:

-   根据 `dbname` 参数创建数据库, 默认为 recommendationengine.
-   `recommendationengine.sql` 创建表结构.
-   `data.sql` 导入基础数据, 比如节目类型字典.
-   `routines.sql` 创建存储过程.
-   `etl_log.viewlog` 代表目前已导入节目数据的最新日期, 格式为 "YYYYMMDD".
    建议选择当前日期的前一个月作为计算起点, 当然前提是收视日志 FTP 服务上已有该天的日志.
-   `etl_log.output` 代表目前已导入收视记录的截止日期, 格式为 "YYYYMMDDHH",
    如何设置, 参见[部署 C2 模块](#bmm_c2)小节.

### 部署数据处理 ETL 任务包

确保成功安装了 Java 7, 否则参见[附录 B](appx02-java)完成.

-   参照附录安装 Kettle ETL 工具.

-   修改最大使用内存的默认设置.
    此处默认安装后为 512, 建议设置成 2048, 最大可以使用 2G 内存.
    编辑文件 `data-integration/kitchen.sh` 如下部分.
```{.texinfo}
if [ -z "$JAVAMAXMEM" ]; then
  JAVAMAXMEM="2048"
fi
```

-   克隆 ETL 流程脚本.
```{.bash}
cd /home/bestv
git clone http://gitlab.bestv.com/bmm/BMM_Modeling.git
cd BMM_Modeling
```

-   将 Kettle 配置文件 `conf/kettle.properties` 拷贝到 `/root/.kettle` 路径下, 如果目录不存在, 请手动创建.
    查看数据库连接信息, 以及 Viewlog (收视日志) 和 Output (节目数据) 的 FTP 连接信息是否设置正确, 请根据实际情况修改.
    **注意**

-   部署统一配置文件.
    根据实际情况, 修改配置文件模板 `BMM_Modeling/conf/settings.properties`, 然后移至 `/home/bestv/conf` 路径下.
    如果目录不存在, 请手动创建.

## 执行建模流程

按下述顺序分别在建模服务器上执行脚本, 分步导入节目数据, 初始化处理收视记录, 并启动每日处理流程.

### 导入节目数据

在建模服务器上执行如下脚本, 在后台运行 `JOB_CSV_Program.kjb`, 完成节目和剧集信息的导入, 清洗, 以及节目 Tag 的计算.

```
nohup /opt/data-integration/kitchen.sh -norep -file=/home/bestv/BMM_Modeling/JOB_Program_Hourly.kjb -param:days='1' &
```

-   查看 `nohup.out`, 获知 ETL 执行情况.
-   查看数据库 program 表和 series 表, 确认分别有几十万条数据, 并且无乱码.

### 导入收视记录

在建模服务器上执行如下脚本, 在后台运行 `JOB_Viewlog_Daily.kjb`,
根据参数 `-param:days='2'` 一次性导入 2 天的 Viewlog 日志, 并完成建模计算.
这里的参数可以根据实际需求进行设置, 如果日志文件很大, 建议第一次运行, 该参数设置小一点,
等到第一次运行成功, 推荐引擎部署完成以后, 再修改该参数, 重新运行, 导入更多日志进行计算.

```
nohup /opt/data-integration/kitchen.sh -norep -file=/home/bestv/BMM_Modeling/JOB_Viewlog_Daily.kjb -param:days='2' &
```

-   查看 `nohup.out`, 获知 ETL 执行情况.
-   查看计算结果表 (以 `model` 打头的表) 的行数, 确保计算顺利完成.

### 启动定时处理流程

在建模服务器上编辑 `vim ~/.cronfile` 文件, 编辑内容如下:

```
LANG='en_US.UTF-8'
9 * * * * /opt/data-integration/kitchen.sh -norep -file=/home/bestv/BMM_Modeling/JOB_Program_Hourly.kjb -param:days='1'
0 3 * * * /opt/data-integration/kitchen.sh -norep -file=/home/bestv/BMM_Modeling/JOB_Viewlog_Daily.kjb -param:days='1'
```

说明:

-   第一行 `LANG='en_US.UTF-8'` 限定了 Cron 所调用任务内部使用的字符集为 UTF-8.
-   第二行代表在每小时第 9 分钟, 调用 C2-Offline 接口数据处理流程, 更新节目数据.
-   第三行代表在每天的 3 点, 调用每日在计算库上进行的建模处理流程.


保存退出, 然后执行以下指令

```{.bash}
crontab ~/.cronfile
```

这样就将 `.cronfile` 文件提交给 cron 进程定时执行.
同时, 新创建 `.cronfile` 的一个副本已经被放在 `/var/spool/cron` 目录中, 文件名就是用户名.
ETL 流程的每日调度设置完成.

## 部署数据库服务器

### 系统环境

-   64 位 的 Red Hat Enterprise Linux Server release 6.x
-   MySQL 5.5 及以上版本

### 安装并配置 MySQL 数据库

同建模服务器数据库的安装和配置.

### 配置 MySQL 主从同步

两台数据库服务器与建模服务器上的 recommendationengine 数据库建立 Master-Slave 架构,
建模服务器的 recommendationengine 数据库为 Master, 数据库服务器上为两个同构的 Slave.
建模服务器上每日数据计算在 Master 数据库 recommendationengine 中进行,
再通过 Master-Slave 架构自动同步到两台 Slave 数据库服务器.

服务器 IP 地址信息如下 (请根据实际情况修改本小节的语句和配置):

-   Master IP: `10.61.13.108`
-   Slave  IP: `10.61.13.109/110`

注意: 主库 MySQL 的版本不能高于从库的版本.

#### 配置主服务器

修改 master 服务器上的配置文件 `/etc/my.cnf`,
在 `[mysqld]` 下添加以下内容, 编辑完毕, 重启 MySQL.

```{.texinfo}
server-id=1
log-bin=master-bin
binlog-ignore-db=mysql,test,information_schema
binlog-do-db=recommendationengine
binlog_format=row
expire_logs_days=3
```

-   `server-id` 必须是 1 到 `2^23-1` 范围内的唯一值.
    主服务器和副服务器的 `server-id` 不能相同.
-   `log-bin` 启用二进制日志, 在这个选项配置日志文件的前缀.
-   `binlog-ignore-db` 日志文件跳过的数据库 (可选属性)
-   `binlog-do-db` 日志文件操作的数据库 (可选属性, 默认所有数据库的相关操作都写入二进制日志文件)
-   `binlog_format` 主从同步机制.
-   `expire_logs_days` 保留 binlog 的天数.

注意: 如果主服务器的二进制日志已经启用, 关闭并重新启动之前应该对以前的二进制日志进行备份.
重新启动后, 应使用 RESET MASTER 语句清空以前的日志.

原因:  master 上对数据库 recommendationengine 的一切操作都记录在日志文件中, 然后会把日志发给 slave,
slave 接收到 master 传来的日志文件之后就会执行相应的操作, 使 slave 中的数据库做和 master 数据库相同的操作.
所以为了保持数据的一致性, 必须保证日志文件没有脏数据.
配置好以上选项后, 重启 MySQL 服务, 新选项将生效.
现在, 所有对数据库中信息的更新操作将被写进日志中

#### 主服务器授权

授权副服务器可以连接主服务器并可以进行更新.
这是在主服务器上进行的, 创建一个 username 和 password 供副服务器访问时使用.
在 MySQL 命令行下输入

```{.mysql}
GRANT REPLICATION SLAVE,RELOAD,SUPER ON *.* TO backup@10.61.13.109 IDENTIFIED BY 'mysql';
GRANT REPLICATION SLAVE,RELOAD,SUPER ON *.* TO backup@10.61.13.110 IDENTIFIED BY 'mysql';
FLUSH PRIVILEGES;
```

这里创建了一个帐号 backup 用于 slave 访问 master 来更新 slave 数据库.



#### 数据复制

将 master 上已有的数据复制到 slave 上, 以便主从数据库建立的时候两个数据库的数据保持一致.
在 master 上导出数据

```{.mysql}
> FLUSH TABLES WITH READ LOCK;
> SHOW MASTER STATUS; # 记录结果备用
$ cd /var/lib/mysql
$ tar -czvf recommendationengine.tar.gz recommendationengine
$ mysqldump -uroot -pmysql -h127.0.0.1 --default-character-set=utf8 -tdR recommendationengine > routines.sql
> UNLOCK TABLES;
```

在 slave 上导入数据

```{.bash}
$ service mysqld stop
$ cd /var/lib/mysql
$ tar -xkzvf recommendationengine.tar.gz
$ service mysqld start
$ mysql -uroot -pmysql -h127.0.0.1 --default-character-set=utf8 recommendationengine
> SOURCE routines.sql
```

另外也可以使用 `LOAD DATA FROM MASTER` 语句将主服务器的数据传输到副服务器, 但使用上有些限制.
注意: 在执行数据复制的过程中, 要确保不能对主服务器执行更新操作. 不推荐使用!

#### 配置从服务器

分别修改 slave 服务器上的配置文件 `/etc/my.cnf`,
在 `[mysqld]` 下添加以下内容, 编辑完毕, 重启 MySQL.

```{.texinfo}
server-id=2/3
relay-log=master-bin
master-retry-count=999
master-connect-retry=60
replicate-do-table=recommendationengine.series
replicate-do-table=recommendationengine.series_ii
replicate-do-table=recommendationengine.series_bak
replicate-do-table=recommendationengine.program
replicate-do-table=recommendationengine.program_ii
replicate-do-table=recommendationengine.program_bak
replicate-do-table=recommendationengine.program_type
replicate-do-table=recommendationengine.modelhottest
replicate-do-table=recommendationengine.modelhottest_ii
replicate-do-table=recommendationengine.modelhottest_bak
replicate-do-table=recommendationengine.modellatest
replicate-do-table=recommendationengine.modellatest_ii
replicate-do-table=recommendationengine.modellatest_bak
replicate-do-table=recommendationengine.modelmanual
replicate-do-table=recommendationengine.modelmostsimilar
replicate-do-table=recommendationengine.modelmostsimilar_ii
replicate-do-table=recommendationengine.modelmostsimilar_tmp
replicate-do-table=recommendationengine.modeluserfavorite
replicate-do-table=recommendationengine.modeluserfavorite_ii
replicate-do-table=recommendationengine.modeluserfavorite_tmp
replicate-do-table=recommendationengine.modelviewalso
replicate-do-table=recommendationengine.modelviewalso_ii
replicate-do-table=recommendationengine.modelviewalso_bak
```

-   `server-id` 唯一并与主服务器上的不同.
-   `replicate-do-table` 复制操作要针对的数据库表.
-   `master-retry-count` 连接重试的次数.
-   `master-connect-retry` 连接失败后等待的秒数.

配置主从同步.
根据之前记录的 `SHOW MASTER STATUS;` 输出结果, 修改下面语句的参数.

```{.mysql}
STOP SLAVE;
CHANGE MASTER TO master_host='10.61.13.108',
      master_user='backup',
      master_password='mysql',
      master_log_file='master-bin.000001',
      master_log_pos=134302;
START SLAVE;
```

副服务器上 MySQL 服务重启后, 还在数据目录中创建一个 `master.info` 文件,
其中包含所有有关复制过程的信息(连接主服务器的相关信息及与主服务器交换数据的相关信息).
在初次启动以后, 副服务器将检查这个 `master.info` 文件, 以得到相关信息.

如果想修改复制选项, 删除 `master.info` 并重启 MySQL 服务, 在启动过程中使用选项配置文件中的新选项进行重新创建了 `master.info` 文件.

将主服务器上备份好的数据文件导入到副服务器数据库中, 以便保证主-副服务器上进行复制操作的起点一样.

#### 查看状态

在 master 上, 查看 master 状态

```{.mysql}
SHOW MASTER STATUS;
````

在 slave 上, 查看 slave 状态

```{.mysql}
SHOW SLAVE STATUS\G
````

截取部分输出

```{.texinfo}
               Slave_IO_State: Waiting for master to send event
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
        Seconds_Behind_Master: 0
```

-   `Slave_IO_State` 显示 `Waiting for master to send event`, 表示已经开启复制功能, 反之, 运行 `START SLAVE;`.
-   `Slave_IO_Running` 显示为 `Yes`, `Slave_SQL_Running` 显示为 `Yes`, 表示主从机能正常备份.
-   `Seconds_Behind_Master` 主从同步延迟
-   暂时停止主从热备份, 运行 `STOP SLAVE;`.

配置主从同步时, 跳过错误的记录条数.

```{.mysql}
STOP SLAVE;
SET GLOBAL SQL_SLAVE_SKIP_COUNTER=1;
START SLAVE;
```

## 部署 Web 服务器

两台 Web 服务器采用完全相同的配置, 通过 F5 提供的虚拟 ip 对外提供基于 http 的智能推荐服务.
每台 web 服务器上配置需要配置 4 个 memcached 实例, 一个 mysql-proxy 实例, 以及一个基于 tomcat 的 java web app 实例.

### 系统环境

-   64 位 的 Red Hat Enterprise Linux Server release 6.x
-   Java 1.7 及以上版本
-   Tomcat 7.0 及以上版本

### 安装应用软件

#### Memcached

-   安装
```{.bash}
yum install -y memcached
```

-   启动
```{.bash}
memcached -d -m 1024 -p 11211 -u root -c 4096
```
启动参数说明:
    -   `-p` 监听的端口
    -   `-d` 以 daemon 形式运行一般皆需增加此参数
    -   `-u` 以何用户身份运行一般选 nobody 等低权用户
    -   `-m` 最大可用内存以兆为单位
    -   `-c` 最大的同时并发数 1024 默认

-   配置.
根据服务器内存情况, 一台服务器上可以启动多个 Memcached 实例, 分别监听不同的端口.
建议开启 4 个实例, 每个实例设置 1G 内存, 端口号依次为:  11211, 11212, 11213, 11214.
在建模服务器中, `/home/bestv/BMM_Modeling/conf` 目录中, 有配置模板,
拷贝到 Web 服务器的 `/home/bestv/conf` 目录下, 重命名为 `memcached.xml`, 然后根据实际情况修改.

    -   `memcached.xml.single` 单实例版
    -   `memcached.xml.multiple` 多实例版

#### MySQL Proxy {#mysql-proxy}

-   安装
```{.bash}
yum install -y mysql-proxy
```

-   启动.
假设两台数据库服务器的 IP 分别为 `10.61.13.109` 和 `10.61.13.110`, MySQL 服务端口都为 3306.
则启动命令如下
```
mysql-proxy --proxy-backend-addresses=10.61.13.109:3306 --proxy-backend-addresses=10.61.13.110:3306 --log-file=/var/log/mysql-proxy/mysql-proxy.log --log-level=error --user=nobody &
```

-   使用.
相当于在本机访问 MySQL, 不过服务端口需改为 `4040`, 可直接使用如下语句登陆数据库

```{.bash}
mysql -uroot -pmysql -h127.0.0.1 --default-character-set=utf8 -P4040
```

#### Tomcat 配置优化

对 Tomcat 进行以下配置, 确保大量 POST 请求能及时释放 HTTP 连接资源.
调整后的配置样例如下:

```{.texinfo}
<Connector port="8080" protocol="HTTP/1.1"
           connectionTimeout="20000"
           maxKeepAliveRequests="1"
           enableLookups="false"
           maxThreads="2000"
           redirectPort="8443" />
```

### 推荐引擎 Web App

在建模服务器中, `/home/bestv/BMM_Modeling/conf` 目录中,
拷贝 `settings.properties` 和 `recommender_config.xml` 文件到 Web 服务器的 `/home/bestv/conf` 目录下.
如果目录不存在, 请手动创建.
接着根据实际情况修改 `settings.properties`, 修改主从数据库的地址, 注意从库的端口为 `4040`, 原因参见 [MySQL Proxy](#mysql-proxy) 小节.

```
cd /opt/apache-tomcat-7.0.47/webapps
wget http://10.61.13.107:8081/job/recommendation/ws/recommendationservice/target/RecommendationService.war
cd ../bin
./startup.sh
```

假设 Web 服务器的 IP 地址为 `10.49.0.34`, 若部署成功, 则可以通过如下地址访问, 智能推荐的一个简单后台页面.

```{.texinfo}
http://10.49.0.34:8080/RecommendationService/testrecommend.html
```

如果希望手动 build, 需要安装 Maven 3, 然后安装 `bestv-memcached-2.5.2.jar` 到本地代码库, 执行如下命令

```
wget ftp://software:bestv123456@10.61.13.97/memcached/bestv-memcached-2.5.2.jar
mvn install:install-file -Dfile=bestv-memcached-2.5.2.jar -DgroupId=com.bestv -DartifactId=bestv-memcached -Dversion=2.5.2 -Dpackaging=jar
```

最后, 克隆源代码, 执行构建命令

```{.bash}
git clone http://gitlab.bestv.com/bmm/BMM_Service.git
cd BMM_Service
mvn -DskipTests clean package -P cf
```

构建成功以后, 将生成 `RecommendationService.war` 文件, 位于 `recommendationservice/target` 下.

## 系统维护说明

### Jar 包说明

-   `BestvCFModeler.jar`
    -   打包: `mvn -DskipTests clean package -P cf`
    -   作用: CF 算法建模
-   `bestv-sync.jar`
    -   打包: `mvn -DskipTests clean package -P c2`
    -   作用: 定时导出 CMS 节目数据

### 系统日志说明

-   建模计算日志
    -   所在服务器: 建模服务器
    -   日志文件路径: `/home/bestv/cmsdata/log/etl`

-   C2 处理日志
    -   所在服务器: 建模服务器
    -   日志文件路径: `/var/log/c2offline.log`

-   推荐访问日志
    -   所在服务器: Web 服务器
    -   日志文件路径: `/home/bestv/cmsdata/log/access`

### 日常维护说明

#### 连接到驻地服务器

以连接福州驻地服务器为例, 首先要添加静态路由, 打开一个终端窗口, 输入以下命令:

```
route -n add -net 10.12.0.0 -netmask 255.255.0.0 10.61.2.2 # Linux
route -p add 10.12.0.0 mask 255.255.0.0 10.61.2.2 # Windows
```

然后就可以用各种终端工具, 比如 SecureCRT, 通过 SSH 方式连接到驻地的服务器.
福州驻地的各服务器信息如下:

-   Web 服务器
    -   Portal-1: `10.12.190.4`, `10.12.190.36`
    -   Portal-2: `10.12.190.5`, `10.12.190.37`
    -   Portal-3: `10.12.190.6`, `10.12.190.38`
-   数据库服务器
    -   DB-1: `10.12.190.7`
    -   DB-2: `10.12.190.8`
-   建模服务器
    -   ETL: `10.12.190.9`

登录帐号均为 `root`, 口令为 `bestvwin`, 如有变动请联系驻地相关人员.

#### 检查 C2Offline 和 Viewlog 数据下发接口

-   C2Offline 通过 ftp 下发节目更新数据, 以福州驻地为例, 具体访问信息如下:
    通过 ftp 访问: <ftp://10.12.190.9>, 用户名和口令均为 `bestvftp`, 每小时两个 CSV 文件.
-   Viewlog 数据也是通过指定的 ftp 服务器下发, 具体访问信息为:
    <ftp://124.108.10.55/Fuzhou/>, 用户名和口令均为 `ftp_mdlog_all`
    收视记录是每天一个文件, 约定每天凌晨 4 点之前要下发完毕最新的日志.

检查上述接口可以确认数据下发是否正常.

#### 检查 C2Offline 和 Viewlog 数据更新状况


#### 检查 ETL 处理日志


#### 检查数据库 Master-Slave 状况

查看数据库的 Master-Slave 状态.

-   在 Master 服务器上执行:  `SHOW MASTER STATUS`;
-   在 Slave 服务器上执行:  `SHOW SLAVE STATUS`;

执行 `SHOW SLAVE STATUS` 命令后, 主要查看以下参数:
`Last_SQL_Error`: 导致错误的 SQL 语句, 正常情况下为空值.



