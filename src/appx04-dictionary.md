# 数据字典

Table: 节目类型及其别名对应关系

| 类型编号 | 节目类型 | 类型别名 |
|:-------:|:--------|:--------|
| 1 | 电视剧 | 电视剧, 连续剧, series |
| 2 | 电影   | 电影, movie |
| 3 | 娱乐   | 娱乐, fun |
| 4 | 财经   | 财经, finance |
| 5 | 纪实   | 纪实, 军事, jishi |
| 6 | 少儿   | 少儿, 哈哈乐园, jiaoyu, children |
| 7 | 生活   | 生活, 生活时尚, 时尚生活, life |
| 8 | 法治   | 法治 |
| 9 | 音乐   | 音乐, yinyue |
| 10 | 凤凰   | 凤凰 |
| 11 | 体育   | 体育, 奥运, sports |


Table: 推荐模型及其说明

| 推荐模型 | 模型说明 |
|:--------|:--------|
| MostSimilarModel | 协同过滤算法 |
| ViewAlsoModel | 关联规则算法 |
| ContentSimilarModel | 基于文本相似度的推荐 |
| UserFavoriteModel | 用户偏好推荐 |
| HottestModel | 最热推荐 |
| LatestModel | 最新推荐 |
| ManualModel | 人工推荐 |


Table: 推荐调用页面及其说明

| 调用页面 | 页面说明 |
|:-----------|:-----|
| `error`                 | 错误页面                 |
| `tv_channel_schedule`   | 节目单                   |
| `user_center`           | 用户中心                 |
| `vod_detail`            | 详情页面                 |
| `vod_play_choice`       | 选片页面                 |
| `vod_series_content`    | 剧集内容                 |
| `vod_series_contiue`    | 继续播放                 |
| `vod_stop_confirm`      | 退出确认                 |
| `vod_stop_thank_you`    | 谢谢观赏                 |
| `channel_stop_confirm`  | 频道退出确认页面         |
| `tvod_stop_confirm`     | 直播回看退出确认         |
| `tvod_stop_thank_you`   | 直播回看谢谢观赏         |