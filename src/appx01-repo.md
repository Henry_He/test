# 软件管理 {#repo}

这里的操作系统为: Red Hat 6, CentOS 6.

## 配置软件源

-   克隆 YUN 源的配置文件
```{.bash}
git clone http://gitlab.bestv.com/he.yongneng/repo.git
cd repo
git checkout v6.5.64.bestv
cp -fa *.repo /etc/yum.repos.d
```

-   更新缓存
```{.bash}
yum clean all
rm -frv /var/cache/yum
yum makecache
```

## 安装常用软件

```{.bash}
yum -y update
yum -y groupinstall 'Development Tools'
yum -y install openssh openssh-server openssh-clients
yum -y install vim tree curl wget curl-devel lsof nodejs mlocate make
yum -y install libffi-devel libxml2-devel libxslt-devel libyaml-devel
yum -y install openssl-devel readline-devel zlib-devel
```