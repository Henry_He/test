# Java

## 安装 Java 7 for Linux

JDK (Java Development Kit) 提供 Java 程序运行所必需的 JRE (Java Run Enironment) 环境,
以及 Java 开发过程中常用的工具包.
这里针对的操作系统为: Ubuntu, Red Hat, CentOS.

-   检查操作系统的位数.
```{.bash}
getconf LONG_BIT
```

-   假设这里为 64 位操作系统, 下载对应的软件包.
```{.bash}
wget ftp://software:bestv123456@10.61.13.97/java/jdk-7u55-linux-x64.gz
```

-   安装软件.
```{.bash}
tar -xkzvf jdk-7u55-linux-x64.gz
mv jdk1.7.0_55 /usr/lib
```

-   配置环境变量.
    向 `/etc/profile` 文件中追加以下内容.
```{.bash}
export JAVA_HOME=/usr/lib/jdk1.7.0_55
export JRE_HOME=$JAVA_HOME/jre
export PATH=$JAVA_HOME/bin:$JRE_HOME/bin:$PATH
export CLASSPATH=.:$JAVA_HOME/lib:$JRE_HOME/lib
```

-   检查是否安装成功.
```{.bash}
source /etc/profile
java -version
```

常见问题

-   权限不够. 提示如下
```{.texinfo}
/usr/lib/jdk1.7.0_55/bin/java: Permission denied
```
为 `java` 命令增加可执行权限, 命令如下
```{.bash}
cd $JAVA_HOME/bin
chmod a+x *
```

-   没有那个文件或目录. 提示如下
```{.texinfo}
/lib/ld-linux.so.2: bad ELF interpreter: No such file or directory
```
Java 的位数与操作系统的位数不匹配, 需下载对应的版本.

-   基于 Java 的软件的汉字显示为方框, 参见 help fonts.

## Tomcat 服务器

-   下载软件.
```{.bash}
cd /opt
wget ftp://software:bestv123456@10.61.13.97/java/apache-tomcat-7.0.47.zip
```

-   安装软件.
```{.bash}
unzip apache-tomcat-7.0.47.zip
cd apache-tomcat-7.0.47/bin
chmod a+x *.sh
```

-   启动服务.
```{.bash}
./startup.sh
```

-   开放 8080 端口, 编辑 `/etc/sysconfig/iptables`, 在相应位置添加下列行
```{.texinfo}
-A INPUT -m state --state NEW -m tcp -p tcp --dport 8080 -j ACCEPT
```
重启防火墙服务 `service iptables restart`.

## Maven 管理器

-   下载软件.
```{.bash}
cd /opt
wget ftp://software:bestv123456@10.61.13.97/java/apache-maven-3.1.1-bin.zip
```

-   安装软件.
```{.bash}
unzip apache-maven-3.1.1-bin.zip
cd apache-maven-3.1.1/bin
ln -s `pwd`/mvn /usr/bin/
```

-   检查是否安装成功.
```{.bash}
mvn -v
```

## JMeter 压力测试

-   下载软件.
```{.bash}
cd /opt
wget ftp://software:bestv123456@10.61.13.97/java/apache-jmeter-2.7.zip
```

-   安装软件.
```{.bash}
unzip apache-jmeter-2.7.zip
```

## Kettle ETL 工具

-   下载软件.
```{.bash}
cd /opt
wget ftp://software:bestv123456@10.61.13.97/data-integration/data-integration.zip
```

-   安装软件.
```{.bash}
unzip data-integration.zip
```



