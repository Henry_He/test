# BesTV智能推荐系统接口技术规范

| 修订号 | 日期 | 描述 | 修订者 |
|:------|:-----|:----|:------|
| 5.0.0 | 2014-08-19 | 按推荐引擎第五版重新编写接口文档, 以 Markdown 格式编写. | 何永能 |

## 文档说明

本文介绍智能推荐系统的数据获取方式、对外提供服务的接口规范，以及日志记录等规范。

### 文档读者

智能推荐系统开发人员、产品部署人员。

### 文档内容组织

第2节整体智能推荐引擎的系统架构，第3节介绍各个接口规范。

### 约定和缩写

| 缩写 | 全称 | 备注 |
|:-----|:----|:-----|
| BMM | Bestv Magic Media | 百视通智能推荐系统 |
| EPG | Electronic Program Guide | 电子节目菜单 |
| CMS | Content Management System | 内容管理系统 |
| 相关推荐 | 相关推荐 | 基于节目的关联推荐 |
| 个性化推荐 | 个性化推荐 | 基于用户的观看记录推荐 |
| viewlog | 用户收视日志 | |
| accesslog | 推荐引擎调用日志 | |
| clicklog | 用户点击日志 | |

\newpage

## 系统描述

### 系统层次结构

-   建模服务器，负责利用节目数据和用户观看记录，计算节目的关联度，形成推荐结果。
-   数据库服务器，通过主从同步方式，将建模服务器上的推荐结果进行复制，并为Web服务器提供数据。
-   Web 服务器，根据用户传入的参数，返回推荐结果。

### 系统架构

![智能推荐的系统架构](image004.png)

### 系统流程概述

1.  推荐引擎通过 `C2_offline` 方式，从驻地CMS获取增量节目数据。
2.  推荐引擎通过FTP方式，从中央BI获取用户观看日志Viewlog。
3.  建模服务器根据上述数据，计算节目之间的关联度，形成推荐结果。
4.  推荐结果保存在主库上，并通过主从同步，复制到两台从库上。
5.  EPG/VIS通过HTTP请求的方式，调用推荐接口，推荐引擎返回结果。

## 系统接口说明

系统的对外接口大致分为三部分: 数据获取接口、 推荐接口和日志记录接口。

### 基于节目的相关性推荐接口

#### 接口交互逻辑

1.  根据 rcount 的值，推荐引擎返回剔除 pviewhistory 后的推荐节目。
2.  剔除以后，如果推荐节目不足 rcount 个，则由 EPG 自行填充。
3.  若推荐引擎无返回结果，或返回错误，则仍由 EPG 自行填充。

#### 接口定义

-   接口协议: HTTP GET
-   协议格式
    -   输入参数说明

    | 参数名 | 类型 | 是否必须 | 说明 | 示例值 |
    |:------|:-----|:--------|:----|:------|
    | method        | String | 必须 | 方法名 | programbasedrecommend |
    | page          | String | 必选 | 接口调用页面名称 | 如: `vod_detail`, 更多参见附表 |
    | uid           | String | 必须 | 当前用户Id | `075447763743` |
    | pid           | String | 必须 | 当前正在观看的节目编号 | `Umai:PROG/116811@BESTV.SMG.SMG` |
    | name          | String | 必须 | 当前正在观看的节目名称 | `命中注定我爱你[1]` |
    | type          | String | 必须 | 当前节目所属的类型 | 如: 电视剧, 更多参见附表 |
    | tag           | String | 可选 | 当前正在观看的节目标签 | `现代;女性;成人`, 分号分隔 |
    | category      | String | 可选 | 当前节目所属的栏目 | |
    | pviewhistory  | String | 可选 | 当前用户的观看历史 | `p2|p3|p4`, 竖线分隔 |
    | bygenre       | String | 可选 | 是否使用节目标签聚焦 | `true`, `false` (默认) |
    | charset       | String | 可选 | 传递参数值的编码 | `UTF-8` (默认) |
    | rcount        | Number | 可选 | 返回的推荐节目个数 | 10 (默认) |
    | format        | String | 可选 | 输出结果的样式 | `xml` (默认), `html` |

    -   输出参数说明 (XML 格式)

    | 标签或属性 | 描述说明 | 示例值 |
    |:----------|:--------|:------|
    | `programbasedrecommend_get_response` | 根节点 | |
    | 　`canrulebefilled` | 是否允许填充推荐内容 | `false` |
    | 　`needselection` | 是否跳转到选片向导 | `false` |
    | 　`recommendation` |  | |
    | 　　`recommenditems` |  | |
    | 　　　`item` |  | |
    | 　　　　`iid` | 推荐节目的编号 | `Umai:SERI/1309515@BESTV.SMG.SMG` |
    | 　　　　`isseries` | 该推荐节目是否为剧头 | `true` |
    | 　　　　`modelid` | 推荐该节目依赖的模型 | `MostSimilarModel`, 更多参见附表 |
    | 　　　　`ischarge` | 该推荐节目是否需付费 | `false` |
    | 　　`programtype` | 源节目的类型 | `1` |
    | 　　`total_results` | 推荐结果的返回数量 | `20` |
    | 　`recid` | 随机产生的推荐跟踪 ID | `-3694359847664648385` |

    -   特别说明:
        EPG在向推荐引擎发起单片推荐请求时，推荐引擎会根据其存储数据判断该片是单片还是连续剧剧集。如果该片确为连续剧剧集，则推荐引擎会主动调用基于剧头的相关性推荐接口，故其此时返回的数据格式是基于剧头相关性推荐接口的数据格式。所以对此接口EPG建议实现为：对接口返回的xml字符串根节点的做统一处理后再行解析。

-   请求及返回样例
    -   请求 (下面的请求地址为 IPTV 智能推荐测试地址, 需根据实际情况进行修改)
```
http://10.61.13.97:8080/RecommendationService/recommend?method=programbasedrecommend&page=vod_detail&uid=075447763743&pid=Umai:PROG/116811@BESTV.SMG.SMG
```
    -   有结果返回
```
<programbasedrecommend_get_response>
    <canrulebefilled>false</canrulebefilled>
    <needselection>false</needselection>
    <recommendation>
        <recommenditems list="true">
            <item><iid>Umai:SERI/1309515@BESTV.SMG.SMG</iid><isseries>true</isseries><modelid>MostSimilarModel</modelid><ischarge>false</ischarge></item>
            ......
        </recommenditems>
        <programtype>1</programtype>
        <total_results>20</total_results>
    </recommendation>
    <recid>-3694359847664648385</recid>
</programbasedrecommend_get_response>
```
    -   无结果返回
```
<programbasedrecommend_get_response>
    <canrulebefilled>false</canrulebefilled>
    <needselection>false</needselection>
    <recommendation>
        <recommenditems list="true"></recommenditems>
        <programtype>0</programtype>
        <total_results>0</total_results>
    </recommendation>
    <recid>-7201845331919829938</recid>
</programbasedrecommend_get_response>
```

### 基于剧头的相关性推荐接口 (不再建议使用)

#### 接口交互逻辑

1.  根据 rcount 的值，推荐引擎返回剔除 pviewhistory 后的推荐节目。
2.  剔除以后，如果推荐节目不足 rcount 个，则由 EPG 自行填充。
3.  若推荐引擎无返回结果，或返回错误，则仍由 EPG 自行填充。

#### 接口定义

-   接口协议: HTTP GET
-   协议格式
    -   输入参数说明

    | 参数名 | 类型 | 是否必须 | 说明 | 示例值 |
    |:------|:-----|:--------|:----|:------|
    | method        | String | 必须 | 方法名 | seriesbasedrecommend |
    | page          | String | 必选 | 接口调用页面名称 | 如: `vod_detail`, 更多参见附表 |
    | uid           | String | 必须 | 当前用户Id | `075447763743` |
    | sid           | String | 必须 | 当前正在观看的节目编号 | `Umai:SERI/808813@BESTV.SMG.SMG` |
    | name          | String | 必须 | 当前正在观看的节目名称 | `命中注定我爱你` |
    | type          | String | 必须 | 当前节目所属的类型 | 如: 电视剧, 更多参见附表 |
    | tag           | String | 可选 | 当前正在观看的节目标签 | `现代;女性;成人`, 分号分隔 |
    | category      | String | 可选 | 当前节目所属的栏目 | |
    | sviewhistory  | String | 可选 | 当前用户的观看历史 | `p2|p3|p4`, 竖线分隔 |
    | bygenre       | String | 可选 | 是否使用节目标签聚焦 | `true`, `false` (默认) |
    | charset       | String | 可选 | 传递参数值的编码 | `UTF-8` (默认) |
    | rcount        | Number | 可选 | 返回的推荐节目个数 | 10 (默认) |
    | format        | String | 可选 | 输出结果的样式 | `xml` (默认), `html` |

    -   输出参数说明 (XML 格式)

    | 标签或属性 | 描述说明 | 示例值 |
    |:----------|:--------|:------|
    | `seriesbasedrecommend_get_response` | 根节点 | |
    | 　`canrulebefilled` | 是否允许填充推荐内容 | `false` |
    | 　`needselection` | 是否跳转到选片向导 | `false` |
    | 　`recommendation` |  | |
    | 　　`recommenditems` |  | |
    | 　　　`item` |  | |
    | 　　　　`iid` | 推荐节目的编号 | `Umai:SERI/1309515@BESTV.SMG.SMG` |
    | 　　　　`isseries` | 该推荐节目是否为剧头 | `true` |
    | 　　　　`modelid` | 推荐该节目依赖的模型 | `MostSimilarModel`, 更多参见附表 |
    | 　　　　`ischarge` | 该推荐节目是否需付费 | `false` |
    | 　　`programtype` | 源节目的类型 | `1` |
    | 　　`total_results` | 推荐结果的返回数量 | `20` |
    | 　`recid` | 随机产生的推荐跟踪 ID | `5050488881301920779` |

-   请求及返回样例
    -   请求 (下面的请求地址为 IPTV 智能推荐测试地址, 需根据实际情况进行修改)
```
http://10.61.13.97:8080/RecommendationService/recommend?method=seriesbasedrecommend&page=vod_detail&uid=075447763743&pid=Umai:SERI/808813@BESTV.SMG.SMG
```
    -   有结果返回
```
<seriesbasedrecommend_get_response>
    <canrulebefilled>false</canrulebefilled>
    <needselection>false</needselection>
    <recommendation>
        <recommenditems list="true">
            <item><iid>Umai:SERI/1309515@BESTV.SMG.SMG</iid><isseries>true</isseries><modelid>MostSimilarModel</modelid><ischarge>false</ischarge></item>
            ......
        </recommenditems>
        <programtype>1</programtype>
        <total_results>20</total_results>
    </recommendation>
    <recid>5050488881301920779</recid>
</seriesbasedrecommend_get_response>
```
    -   无结果返回
```
<seriesbasedrecommend_get_response>
    <canrulebefilled>false</canrulebefilled>
    <needselection>false</needselection>
    <recommendation>
        <recommenditems list="true"></recommenditems>
        <programtype>0</programtype>
        <total_results>0</total_results>
    </recommendation>
    <recid>2791910681821219012</recid>
</seriesbasedrecommend_get_response>
```

### 基于观看历史的个性化推荐接口

#### 接口交互逻辑

1.  若系统判断为新用户，则由EPG跳转到用户类型偏好提交接口。
2.  若系统判断为老用户，则进行个性化推荐。

#### 接口定义

-   接口协议: HTTP GET
-   协议格式
    -   输入参数说明

    | 参数名 | 类型 | 是否必须 | 说明 | 示例值 |
    |:------|:-----|:--------|:----|:------|
    | method        | String | 必须 | 方法名 | personalizedrecommend |
    | page          | String | 必选 | 接口调用页面名称 | 如: `user_center`, 更多参见附表 |
    | uid           | String | 必须 | 当前用户Id | `075447763743` |
    | pviewhistory  | String | 可选 | 当前用户的观看历史 | `p2|p3|p4`, 竖线分隔 |
    | charset       | String | 可选 | 传递参数值的编码 | `UTF-8` (默认) |
    | rcount        | Number | 可选 | 返回的推荐节目个数 | 10 (默认) |
    | format        | String | 可选 | 输出结果的样式 | `xml` (默认), `html` |

    -   输出参数说明 (XML 格式)

    | 标签或属性 | 描述说明 | 示例值 |
    |:----------|:--------|:------|
    | `personalizedrecommend_get_response` | 根节点 | |
    | 　`canrulebefilled` | 是否允许填充推荐内容 | `false` |
    | 　`needselection` | 是否跳转到选片向导 | `false` |
    | 　`recommendation` |  | |
    | 　　`recommenditems` |  | |
    | 　　　`item` |  | |
    | 　　　　`iid` | 推荐节目的编号 | `Umai:SERI/1285994@BESTV.SMG.SMG` |
    | 　　　　`isseries` | 该推荐节目是否为剧头 | `true` |
    | 　　　　`modelid` | 推荐该节目依赖的模型 | `UserFavoriteModel`, 更多参见附表 |
    | 　　　　`ischarge` | 该推荐节目是否需付费 | `false` |
    | 　　`preference` | 用户的偏好标签 | `<![CDATA[言情剧|传记剧|偶像剧|动作|喜剧]]>` |
    | 　　`total_results` | 推荐结果的返回数量 | `20` |
    | 　`recid` | 随机产生的推荐跟踪 ID | `-5032921449271243175` |

    -   特别说明:
        EPG调用接口一次，获取N个推荐节目，然后再分页显示。每次分页显示不必再调用接口。对同一个用户，接口每次返回的结果都会有随机性差异，比如调用需要返回32个，则推荐系统会选取最感兴趣的64个，随机挑选32个。

-   请求及返回样例
    -   请求 (下面的请求地址为 IPTV 智能推荐测试地址, 需根据实际情况进行修改)
```
http://10.61.13.97:8080/RecommendationService/recommend?method=personalizedrecommend&page=user_center&uid=075447763743
```
    -   有结果返回
```
<personalizedrecommend_get_response>
    <canrulebefilled>false</canrulebefilled>
    <needselection>false</needselection>
    <recommendation>
        <recommenditems list="true">
            <item><iid>Umai:SERI/1285994@BESTV.SMG.SMG</iid><isseries>true</isseries><modelid>UserFavoriteModel</modelid><ischarge>false</ischarge></item>
            ......
        </recommenditems>
        <preference><![CDATA[言情剧|传记剧|偶像剧|动作|喜剧]]></preference>
        <programtype>0</programtype>
        <total_results>20</total_results>
    </recommendation>
    <recid>-5032921449271243175</recid>
</personalizedrecommend_get_response>
```
    -   无结果返回, 如果没有观看记录, 会推出最新最热节目, 因此不存在无结果返回的情况.

### 最热节目推荐接口

#### 接口交互逻辑

-   根据 rcount 的值, 以及 type 的值, 返回该节目类型的最热节目排行榜.
-   如果 type 是汉字, 需要用 `urlencode` 转换.

#### 接口定义

-   接口协议: HTTP GET
-   协议格式
    -   输入参数说明

    | 参数名 | 类型 | 是否必须 | 说明 | 示例值 |
    |:------|:-----|:--------|:----|:------|
    | method        | String | 必须 | 方法名 | hottestrecommend |
    | page          | String | 必选 | 接口调用页面名称 | 如: `user_center`, 更多参见附表 |
    | type          | String | 必须 | 当前节目所属的类型 | 如: 电视剧, 更多参见附表 |
    | charset       | String | 可选 | 传递参数值的编码 | `UTF-8` (默认) |
    | rcount        | Number | 可选 | 返回的推荐节目个数 | 10 (默认) |
    | format        | String | 可选 | 输出结果的样式 | `xml` (默认), `html` |

    -   输出参数说明 (XML 格式)

    | 标签或属性 | 描述说明 | 示例值 |
    |:----------|:--------|:------|
    | `hottestrecommend_get_response` | 根节点 | |
    | 　`canrulebefilled` | 是否允许填充推荐内容 | `false` |
    | 　`needselection` | 是否跳转到选片向导 | `false` |
    | 　`recommendation` |  | |
    | 　　`recommenditems` |  | |
    | 　　　`item` |  | |
    | 　　　　`iid` | 推荐节目的编号 | `Umai:PROG/1119712@BESTV.SMG.SMG` |
    | 　　　　`isseries` | 该推荐节目是否为剧头 | `false` |
    | 　　　　`modelid` | 推荐该节目依赖的模型 | `HottestModel`, 更多参见附表 |
    | 　　　　`ischarge` | 该推荐节目是否需付费 | `false` |
    | 　　`programtype` | 源节目的类型 | `0` |
    | 　　`total_results` | 推荐结果的返回数量 | `20` |
    | 　`recid` | 随机产生的推荐跟踪 ID | `-3503963567072706427` |

-   请求及返回样例
    -   请求 (下面的请求地址为 IPTV 智能推荐测试地址, 需根据实际情况进行修改)
```
http://10.61.13.97:8080/RecommendationService/recommend?method=hottestrecommend&page=user_center&type=%E7%94%B5%E5%BD%B1
```
    -   有结果返回
```
<hottestrecommend_get_response>
    <canrulebefilled>false</canrulebefilled>
    <needselection>false</needselection>
    <recommendation>
        <recommenditems list="true">
            <item><iid>Umai:PROG/1119712@BESTV.SMG.SMG</iid><isseries>false</isseries><modelid>HottestModel</modelid><ischarge>false</ischarge></item>
            ......
        </recommenditems>
        <programtype>0</programtype>
        <total_results>20</total_results>
    </recommendation>
    <recid>-3503963567072706427</recid>
</hottestrecommend_get_response>
```

### 最新节目推荐接口

#### 接口交互逻辑

-   根据 rcount 的值, 以及 type 的值, 返回该节目类型的最新节目排行榜.
-   如果 type 是汉字, 需要用 `urlencode` 转换.

#### 接口定义

-   接口协议: HTTP GET
-   协议格式
    -   输入参数说明

    | 参数名 | 类型 | 是否必须 | 说明 | 示例值 |
    |:------|:-----|:--------|:----|:------|
    | method        | String | 必须 | 方法名 | latestrecommend |
    | page          | String | 必选 | 接口调用页面名称 | 如: `user_center`, 更多参见附表 |
    | type          | String | 必须 | 当前节目所属的类型 | 如: 电视剧, 更多参见附表 |
    | charset       | String | 可选 | 传递参数值的编码 | `UTF-8` (默认) |
    | rcount        | Number | 可选 | 返回的推荐节目个数 | 10 (默认) |
    | format        | String | 可选 | 输出结果的样式 | `xml` (默认), `html` |

    -   输出参数说明 (XML 格式)

    | 标签或属性 | 描述说明 | 示例值 |
    |:----------|:--------|:------|
    | `latestrecommend_get_response` | 根节点 | |
    | 　`canrulebefilled` | 是否允许填充推荐内容 | `false` |
    | 　`needselection` | 是否跳转到选片向导 | `false` |
    | 　`recommendation` |  | |
    | 　　`recommenditems` |  | |
    | 　　　`item` |  | |
    | 　　　　`iid` | 推荐节目的编号 | `Umai:PROG/1418025@BESTV.SMG.SMG` |
    | 　　　　`isseries` | 该推荐节目是否为剧头 | `false` |
    | 　　　　`modelid` | 推荐该节目依赖的模型 | `LatestModel`, 更多参见附表 |
    | 　　　　`ischarge` | 该推荐节目是否需付费 | `true` |
    | 　　`programtype` | 源节目的类型 | `0` |
    | 　　`total_results` | 推荐结果的返回数量 | `20` |
    | 　`recid` | 随机产生的推荐跟踪 ID | `-527025404289721195` |

-   请求及返回样例
    -   请求 (下面的请求地址为 IPTV 智能推荐测试地址, 需根据实际情况进行修改)
```
http://10.61.13.97:8080/RecommendationService/recommend?method=latestrecommend&page=user_center&type=%E7%94%B5%E5%BD%B1
```
    -   有结果返回
```
<latestrecommend_get_response>
    <canrulebefilled>false</canrulebefilled>
    <needselection>false</needselection>
    <recommendation>
        <recommenditems list="true">
            <item><iid>Umai:PROG/1418025@BESTV.SMG.SMG</iid><isseries>false</isseries><modelid>LatestModel</modelid><ischarge>true</ischarge></item>
            ......
        </recommenditems>
        <programtype>0</programtype>
        <total_results>20</total_results>
    </recommendation>
    <recid>-527025404289721195</recid>
</latestrecommend_get_response>
```
