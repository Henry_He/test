# 版本信息

## 2014-11-28 `bmm_5.1.7`

### 功能特性

-   增加 GenreSorter, 在最后的推荐结果中, 按照类型等属性再排序.
-   删除冗余的 Log4j 日志记录配置信息.
-   不允许推荐相同名字的节目; 不允许高清推荐标清节目.
-   修复 Lucene 查询时空指针问题, 目标实现现网零异常.
-   修复 AccessLog 日志记录信息缺失问题.

### 升级方案

#### Web 服务器

-   `BMM_Service_5.1.2 => BMM_Service_5.1.3`
    -   依次操作每台 Web 服务器, 替换 RecommendationService.war, 重启 Tomcat.

## 2014-10-21 `bmm_5.1.6`

### 功能特性

-   配合蓝宝石 2.0 上线, ItemRelation 增加 Id 字段, 作为表主键.
-   强化数据清洗, 引入节目元信息的 Length, Genre 字段, 作为数据清洗的依据之一.
-   调整推荐顺序: `view_also`, `most_similar`, `hottest`, `latest`.
-   协同过滤打分数据上调至 5 组, 关联规则打分数据下调至 5 组.
-   配合最新节目推荐算法, 节目的时间信息精确到秒.
-   增加 CF 建模所需的内存至 2 GB.
-   Access 日志增加日期时间戳.
-   不再推荐第三方的节目.

### 升级方案

#### 建模服务器

-   准备工作
    -   确保没有正在执行的 ETL 流程. (`ps aux | grep kitchen`)
    -   关闭 ETL 定时任务.

-   `BMM_C2_5.1.0 => BMM_C2_5.1.1`
    -   仅需替换两个文件 `database.properties` 和 `VERSION`.
    -   kill 进程 `bestv-sync.jar`.
    -   进入 `/view_log` 目录, 删除历史文件 `rm -frv program_*.csv series_*.csv`.
    -   进入 `/home/bestv/BMM_C2/conf` 目录, **一定要重命名** `database.properties` 为 `database.properties.old`. (用于回退)
    -   解压缩 `BMM_C2_5.1.1_build20141021.zip`, 进入 `BMM_C2/conf` 目录, 根据平台选择拷贝模板文件 `database.properties.xxx`, 到 `/home/bestv/BMM_C2/conf` 目录下.
        重命名为 `database.properties`, 参照 `database.properties.old`, 修改 Oracle 连接信息, 主要是 IP 和字符集编码.
    -   拷贝覆盖掉 `VERSION` 文件.
    -   进入 `/home/bestv/BMM_C2` 目录, 删除隐藏文件 `.updatetime`.
    -   手动执行导出命令 `java -jar bestv-sync.jar sync`, 根据日志确定是否导出成功, 并记录导出文件名的时间戳的前十位数字减去1.
    -   执行启动命令 `./start`.

-   `BMM_DB_5.0.1 => BMM_DB_5.0.2`
    -   进入 `/home/bestv` 工作目录, 重命名 `BMM_DB` 为 `BMM_DB.old`. (用于回退)
    -   拷贝 `BMM_DB_5.0.2_build20141021.zip` 到 `/home/bestv` 工作目录, 然后解压缩.
    -   进入 `BMM_DB`, 登陆数据库 `mysql -uroot -pmysql -h127.0.0.1 --default-character-set=utf8 recommendationengine`.
    -   依次执行语句 `source 20140924131232_add_primary_key.sql`, `source 20141021134902_add_length_column.sql`.
    -   根据升级 C2 模块时记录的时间戳, 修改并执行语句 `update etl_log set StatDate='2014xxxxxx' where ETLName='output';`.

-   `BMM_Modeling_5.1.2 => BMM_Modeling_5.1.3`
    -   进入 `/home/bestv` 工作目录, 重命名 `BMM_Modeling` 为 `BMM_Modeling.old`. (用于回退)
    -   拷贝 `BMM_Modeling_5.1.3_build20141021.zip` 到 `/home/bestv` 工作目录, 然后解压缩.
    -   执行节目元数据导入流程. (命令可查看定时任务配置)
    -   进入 `/home/bestv/BMM_DB` 目录, 登陆数据库, 执行语句 `source 20141023105342_clean_series.sql`

-   结束工作
    -   开启 ETL 定时任务.

#### Web 服务器

-   `BMM_Service_5.1.1 => BMM_Service_5.1.2`
    -   **多台同时操作** (防止 Memcache 干扰), 替换 RecommendationService.war, 清空网页缓存目录 work, 关闭 Tomcat, **注意这里先不开启 Tomcat**.
    -   重启 Memcache.
    -   开启 Tomcat.

## 2014-09-24 `bmm_5.1.5`

### 功能特性

-   索引节目信息时, 剔除节目名为 NULL 的情形;
-   访问日志增加 User.Id, User.MediaCode, page 的记录;
-   若节目推荐时缺少节目信息, 则转为个性化推荐.

### 升级方案

-   `BMM_Service_5.1.0 => BMM_Service_5.1.1`
    -   替换 **Web 服务器** Tomcat 下的 RecommendationService.war
    -   重启 Tomcat

## 2014-09-12 `bmm_5.1.4`

### 功能特性

-   C2 接口, 增加数据库字符集设置

### 升级方案

-   `BMM_C2: 5.0.0 => 5.1.0`
    -   替换 `bestv-sync.jar`.
    -   替换 `config/applicationContext.xml`, 如果是 IPTV 平台, 还需编辑 `applicationContext.xml` 文件, 取消第 77 行的注释.
    -   编辑 `config/database.properties`, 增加字符集设置, 如 `oracle.charset=ISO-8859-1`.

## 2014-08-15 `bmm_5.1.3`

### 功能特性

-   测试 Bug 修复, 不再推出 "导视", "预告" 之类的片子.

### 升级方案

-   `BMM_Modeling: 5.1.0 => 5.1.2`
    -   整体替换 `/home/bestv/BMM_Modeling`.
    -   拷贝文件 `/home/bestv/BMM_Modeling/conf/recommender_config.xml` 到 Web 服务器上, 替换掉 `/home/bestv/conf` 下的相应文件.

## 2014-07-29 `bmm_5.1.0`

### 功能特性

-   直播回看推荐, `liverecommend`.
-   首个付费节目置顶, `ChargeSorter`.
-   最热最新排行榜, `HottestRecommender` 和 `LatestRecommender`.

### 升级方案

-   `BMM_Modeling: 5.0.0 => 5.1.0`
    -   整体替换 `/home/bestv/BMM_Modeling`.
    -   拷贝文件 `/home/bestv/BMM_Modeling/conf/recommender_config.xml` 到 Web 服务器上, 替换掉 `/home/bestv/conf` 下的相应文件.
-   `BMM_Service: 5.0.0 => 5.1.0`
    -   替换 `RecommendationService.war`
-   `BMM_DB: 5.0.0 => 5.0.1`
    -   登陆 `recommendationengine` 数据库, `mysql -uroot -pmysql -h127.0.0.1 --default-character-set=utf8 recommendationengine`.
    -   执行 `source routines.sql.` 语句.

## 2014-07-18 `bmm_5.0.0`

-   重写 BMM, 拆分成四个模块: BMM_DB, BMM_C2, BMM_Modeling, BMM_Service.
-   自增长整数 ID, 优化数据库查询.
-   轻量化流程, 删除冗余计算流程.
-   整合 ETL 流程, 合并节目元信息处理流程.